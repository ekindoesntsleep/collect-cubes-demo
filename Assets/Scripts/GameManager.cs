﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public Slider slider;
    public float maxCubeCount;
    public float cubesLeft;
    public float cubesCollected;

    public void Start()
    {
        instance = this;
    }

    public static void CheckEndCondition()
    {
        instance.slider.value = instance.cubesCollected / instance.maxCubeCount;
        EndLevel();
    }

    public static void EndLevel()
    {
        if (instance.cubesLeft <= 0)
        {
            SceneManager.LoadScene(0);
        }
    }
}

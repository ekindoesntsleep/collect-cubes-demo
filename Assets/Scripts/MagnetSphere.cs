﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetSphere : MonoBehaviour
{
    public Material coloredCubeMaterial;

    private void OnTriggerEnter(Collider other)
    {
        GameManager.instance.cubesLeft--;
        GameManager.instance.cubesCollected++;
        GameManager.CheckEndCondition();

        if (other.gameObject.layer == 11)
        {
            other.gameObject.layer = 14;
            StartCoroutine(ApplyForceUntilSleeping(other.gameObject));
            other.GetComponent<MeshRenderer>().material = coloredCubeMaterial;
        }
    }

    public IEnumerator ApplyForceUntilSleeping(GameObject other)
    {
        Rigidbody otherRigidbody = other.GetComponent<Rigidbody>();
        do
        {
            otherRigidbody.AddForce((gameObject.transform.position - other.transform.position) * 1000, ForceMode.Force);
            yield return null;
        }
        while (otherRigidbody.velocity.sqrMagnitude > 0.1f);

        Destroy(otherRigidbody);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    Vector2 previousMousePosition;
    Vector2 currentMousePosition;
    Vector2 clickPosition;
    bool initialTouch = true;

    Vector2 differenceBetweenMousePositions;
    float playerFacingDirectionX;
    float playerFacingDirectionY;

    Vector3 playerRotation;
    Vector3 playerDirection;

    void Start()
    {
        previousMousePosition = Vector2.zero;
        currentMousePosition = Vector2.zero;
    }

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            InitiateClicking();
            HandleMovement();
            HandleRotation();
        }

        FinalizeClicking();
    }

    void InitiateClicking()
    {
        clickPosition = Input.mousePosition;
        currentMousePosition = clickPosition;

        if (initialTouch)
        {
            previousMousePosition = currentMousePosition;
            initialTouch = false;
        }
    }

    void HandleMovement()
    {
        differenceBetweenMousePositions = currentMousePosition - previousMousePosition;
        playerFacingDirectionX = differenceBetweenMousePositions.x;
        playerFacingDirectionY = differenceBetweenMousePositions.y;

        playerRotation = new Vector3(playerFacingDirectionX, 0, playerFacingDirectionY);
        playerDirection = new Vector3(-playerFacingDirectionY, 0, playerFacingDirectionX);

        GetComponent<Rigidbody>().AddForce(playerDirection * 60, ForceMode.Force);        
    }

    void HandleRotation()
    {
        transform.LookAt(transform.position + (Vector3)playerRotation);
    }    

    void FinalizeClicking()
    {
        if (Input.GetMouseButtonUp(0))
        {
            initialTouch = true;
        }

        previousMousePosition = currentMousePosition;
    }
}
